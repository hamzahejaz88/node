const express = require("express");
const router = express.Router();
const Authenticate = require("../Middlewares/Authenticate");

const GetRoutes = require("../Controller/Controller");

router.get("/Doctor", GetRoutes.func);
router.get("/Patients",GetRoutes.pat)
router.get("/Departments", GetRoutes.Deps);
router.get("/Doctors:department", GetRoutes.docdep);
router.get("/Nurse", GetRoutes.role);
router.get("/Patient:name", GetRoutes.nurpat);
router.post("/patient", GetRoutes.add);
router.post("/Staff", GetRoutes.staff);
router.post("/Login", GetRoutes.Login);
router.patch("/Update:id", GetRoutes.Update);
router.post("/UpdatePatient:id", GetRoutes.UpdatePateient);
router.delete("/Delete:id", GetRoutes.Delete);
router.delete("/DeletePatient:id", GetRoutes.DeletePatient);
router.post("/Admin", GetRoutes.Admin, Authenticate);
router.post("/Department",GetRoutes.Dep)
router.get("/Dashboard:id", GetRoutes.Dash);
module.exports = router;
