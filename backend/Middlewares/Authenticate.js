const jwt = require("jsonwebtoken");

module.exports = function (req, res, next) {
  const auth = req.header("auth-token");
  if (!auth) return res.status(400).send({ message: "Acees denied" });

  try {
    const verified = jwt.verify(auth, process.env.Token_Secret);
    res.User = verified;
    next();
  } catch (error) {
    res.status(400).send("Invalid Token");
  }
};
