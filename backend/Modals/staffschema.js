const mongoose = require("mongoose");

var StaffSchema = new mongoose.Schema({
  name: String,
  age: String,
  phone: String,
  address: String,
  username: String,
  password: String,
  role: String,
  
  email: String,
  gender: String,
  department: String,
  startdate:String,
  enddate:String,
  date: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model("Staff", StaffSchema);
