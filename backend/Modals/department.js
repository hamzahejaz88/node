const mongoose = require("mongoose");

var DepSchema = new mongoose.Schema({
  name: String,
 
  date: {
    type: Date,
    default: Date.now,
  },
});

module.exports = mongoose.model("Departments", DepSchema);
