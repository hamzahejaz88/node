const { string } = require("@hapi/joi");
var mongoose = require("mongoose");

var Patient = new mongoose.Schema({
  name: String,
  age: String,
  phone: String,
  doc: String,
  disease: String,
  address: String,
  gender: String,
  date: String,
  nur:String,
  amount:{
    type:String,
    default:null
  },
  status:String,
  prescription:String
  // date:{sas
  //     type:Date,
  //     default:Date.now
  // },
  //    appointment:[
  //        {
  //         docid:string,
  //         type:Date,
  //         default:Date.now
  //        }
  //    ]
});

module.exports = mongoose.model("Patient", Patient);
