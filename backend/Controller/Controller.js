const express = require("express");
const bcrypt = require("bcryptjs");
const Patient = require("../Modals/userschema");
const Staff = require("../Modals/staffschema");
const jwt = require("jsonwebtoken");
const Department = require ("../Modals/department")
const { RegistrationValidaton, Doc, Login } = require("./Validation");

const GetRoutes = {};

GetRoutes.func = async (req, res) => {

  try {
    const get = await Staff.find({ role: "Doctor" });
    res.json(get);
  } catch (error) {
    console.log(error);
    res.json({ message: error });
  }
};
GetRoutes.Deps = async(req,res) =>{
  const deps = await Department.find();
  res.json(deps)
}
GetRoutes.pat = async(req,res) =>{
  const ptnt = await Patient.find()
  res.json(ptnt)
}
GetRoutes.docdep = async (req, res) => {
  const dep=req.params.department
  try {
    const get = await Staff.find({ role: "Doctor" ,department:dep});
    res.json(get);
  } catch (error) {
    console.log(error);
    res.json({ message: error });
  }
};
GetRoutes.nurpat = async (req, res) => {
  const name=req.params.name
  try {
    const get = await Patient.find({ nur:name});
    res.json(get);
  } catch (error) {
    console.log(error);
    res.json({ message: error });
  }
};
GetRoutes.role = async (req, res) => {
  try {
    const get = await Staff.find({ role: "Nurse" });
    res.json(get);
  } catch (error) {
    console.log(error);
    res.json({ message: error });
  }
};
GetRoutes.Departments =async(req,res) =>{
  const department = await Department.find()
  res.json(department)
}
GetRoutes.add = async (req, res) => {
  try {
    const { error } = RegistrationValidaton(req.body);

    if (error) {
      return res.status(400).json(error);
    }
  } catch (err) {
    console.log(err);
    return res.status("500").send({ message: "Patient Validation Error" });
  }
  console.log("Pateint data is", req.body);
  const patnt = new Patient({
    name: req.body.name,
    age: req.body.age,
    phone: req.body.phone,
    address: req.body.address,
    doc: req.body.doc,
    gender: req.body.gender,
    date: req.body.date,
    disease: req.body.disease,
    nur:req.body.nur,
    status:req.body.status,
    amount:req.body.amount,
    prescription:req.body.prescription

  });
  try {
    const savedPatient = await patnt.save();
    res.json(savedPatient);
  } catch (error) {
    res.json({ message: error });
  }
};

GetRoutes.staff = async (req, res) => {
  console.log("Start req add is",req.body)
  try {
    
    const emailExists = await Staff.findOne({ email: req.body.email });
    if (emailExists)
      return res.status(400).json({ message: "Email already exists" });
    const username = await Staff.findOne({ username: req.body.username });
    if (username)
      return res.status(400).send({ message: "Username already exists" });
    return res.status(400).send(error);
  } catch (error) {
    res.json(error);
  }

  const salt = await bcrypt.genSalt(10);
  const hashedpassword = await bcrypt.hash(req.body.password, salt);
  console.log(req.body);
  const employee = new Staff({
    name: req.body.name,
    age: req.body.age,
    phone: req.body.phone,
    address: req.body.address,
    username: req.body.username,
    password: hashedpassword,
    email: req.body.email,
    role: req.body.role,
    gender: req.body.gender,
    department: req.body.department,
    startdate:req.body.startdate,
    enddate:req.body.enddate,
  });
  try {
    const savedemp = await employee.save();
    res.json(savedemp);
  } catch (error) {
    console.log(error);
    res.send({ message: error });
  }
};

GetRoutes.Login = async (req, res) => {
  const { error } = Login(req.body);
  if (error) return res.status(400).send(error);
  const staff = await Staff.findOne({ username: req.body.username });
  if (!staff) return res.status(400).send({ Error: "invalid Email" });

  const validatepass = await bcrypt.compare(req.body.password, staff.password);
  if (!validatepass) return res.status(400).send({ Error: "invalid Password" });

  const token = jwt.sign({ _id: Staff._id }, process.env.Token_Secret);
  const results = await Staff.find({ username: req.body.username });

  res
    .header("auth-token", token)
    .send({ auths: true, token: token, Data: results });
};
GetRoutes.Admin = async (req, res) => {
  try {
    res.json({ User: true });
  } catch (error) {
    res.json(error);
  }
};
GetRoutes.Update = async (req, res) => {
  console.log(req.params.id);
  console.log(req.body);
  const id = req.params.id;
  Staff.findOne({ _id: id }, function (err, foundObject) {
    if (err) {
      console.log(err);
      res.status(500).send("Sorry");
    } else {
      if (!foundObject) {
        res.status(404).send("Wrong id");
      } else {
        if (req.body.username) {
          foundObject.username = req.body.username;
        }
        if (req.body.name) {
          foundObject.name = req.body.name;
        }
        if (req.body.email) {
          foundObject.email = req.body.email;
        }
        if (req.body.address) {
          foundObject.address = req.body.address;
        }
        if (req.body.phone) {
          foundObject.phone = req.body.phone;
        }
        if (req.body.startdate) {
          foundObject.startdate = req.body.startdate;
        }
        if (req.body.enddate) {
          foundObject.enddate = req.body.enddate;
        }
        foundObject.save(function (err, updatedObject) {
          if (err) {
            console.log(err);
            res.status(500).send("Saving error");
          } else {
            res.send(updatedObject);
          }
        });
      }
    }
  });
};
GetRoutes.UpdatePateient = async (req, res) => {
  console.log("Requested Update Patient id is", req.params.id);
  const id = req.params.id;
  Patient.findOne({ _id: id },  function (error, foundObject) {
    if (error) {
      res.json(error);
    } else {
      if (!foundObject) {
        res.json({ message: "Invalid Patient Id" });
      } else {
        if (req.body.name) {
          foundObject.name = req.body.name;
        }
        if (req.body.status) {
          foundObject.status = req.body.status;
        }
        if (req.body.amount) {
          foundObject.amount = req.body.amount;
        }
        if (req.body.prescription) {
          foundObject.prescription = req.body.prescription;
        }

        foundObject.save(function (err, updatedObject) {
          if (err) {
            console.log(err);
            res.status(500).send("Saving error");
          } else {
            res.send(updatedObject);
          }
        });
      }
    }
  });
};
GetRoutes.Delete = async (req, res) => {
  const id = req.params.id;
  console.log("Delete id is", id);
  try {
    const deletepost = await Staff.findByIdAndRemove(id);
    return res.json(deletepost).send("Succefully Deleted");
  } catch (err) {
    console.log(err);
    res.json({ message: err });
  }
};
GetRoutes.DeletePatient = async (req, res) => {
  const id = req.params.id;
  console.log("Delete id is", id);
  try {
    const deletepost = await Patient.findByIdAndRemove(id);
    return res.json(deletepost).send("Succefully Deleted");
  } catch (err) {
    console.log(err);
    res.json({ message: err });
  }
};
GetRoutes.Doctors = async (req, res) => {
  try {
    const doc = await Staff.find({});
    res.json(doc);
  } catch (error) {
    res.json({ message: error }).send("Can't get the Doctors");
  }
};
GetRoutes.Dash = async (req, res) => {
  const id = req.params.id;
 
  try {
    const pateient = await Patient.find({ doc: id });
    res.json(pateient).send("Succesfull");
  } catch (err) {
    res.status('400').send(err);
  }
};
GetRoutes.Dep = async(req,res) =>{
console.log(req.body);
try {
  const depExists = await Department.findOne({ name: req.body.name });
  if (depExists)
    return res.status(400).json({ message: "Department already exists" });
} catch (error) {
  res.json(error)
}
const deps=new Department ({
  
    
    name:req.body.name 
  
})
try {
  const savedep = await deps.save()
  res.json(savedep)
} catch (error) {
  console.log(error);
  res.json(error)
}
}
module.exports = GetRoutes;
