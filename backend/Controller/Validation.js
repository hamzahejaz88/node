const Joi = require("@hapi/joi");

const RegistrationValidaton = (data) => {
  console.log("Registration Validation is", data);
  const schema = Joi.object({
    name: Joi.string().required(),
    disease: Joi.string().required(),
    phone: Joi.string().required(),
    age: Joi.string().required(),
    doc: Joi.string().required(),
    nur:Joi.string().required(),
    gender: Joi.string().required(),
    address: Joi.string().required().max(50),
    date: Joi.string(),
    status:Joi.string(),
    amount:Joi.string(),
    prescription:Joi.string()
  });

  return Joi.validate(data, schema);
};
const Login = (data) => {
  const login = Joi.object({
    username: Joi.string().required(),
    password: Joi.string().required().trim(),
  });
  return Joi.validate(data, login);
};

const Doc = (data) => {
  const Emp = Joi.object({
    name: Joi.string().required().trim().min(3).max(25),
    age: Joi.string().required('_'),
    phone: Joi.string().trim(' '),
    address: Joi.string().required().max(50),
    username: Joi.string().required(),
    password: Joi.string().required(),
    role: Joi.string().required(),
    department: Joi.string().required(),
    email: Joi.string().required(),
    gender:Joi.string(),
    startdate:Joi.string(),
    enddate:Joi.string(),
  });
  return Joi.validate(data, Emp);
};

module.exports.RegistrationValidaton = RegistrationValidaton;
module.exports.Doc = Doc;
module.exports.Login = Login;
